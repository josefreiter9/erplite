# ERPLite - Svelte - AA1  - THOMAS HOFMANN und Josef REITER

## ***ALLGEMEINES***
---
---

|**PROJEKTBESCHREIBUNG**||
| :-| :-|
|---|---|
|**PROJEKTBETREUER**|THOMAS HOFMANN|
||JOSEF REITER|
|**JAHRGANG / KLASSE**|6AKIF|
|**LEHRPERSON**|Dr. CLAUDIO LANDERER|
|---|---|
|**PROJEKTBESCHREIBUNG**|**A) Abgabe der Architekturanalyse des bestehenden erplite-Backends**|
||**A1) Dokumentation (schriftlich, Codeauszüge, C4-Diagramme, Klassendiagramme) der Ports-Und-Adapters-Architektur von Ordermanagement anhand der gegebenen Anwendungsfälle, die schon implementiert sind:**|
||- Bestellung aufgeben, Bestellung auf bezahlt setzen, Packliste generieren, Packlistenitems abhaken, Bestellung auf IN_DELIVERY setzen wenn alle Packlistenitems gepackt sind|
||**A2) Dokumentation (schriftlich, Codeauszüge, Diagramme, C4-Diagramme, Klassendiagramme) der "Architektur" von Stockmanagement anhand der gegebenen Anwendungsfälle, die schon implementiert sind:**|
||Packingliste anlegen, Packingitems als verpackt markieren|
||**B) Abgabe des Frontends für das bestehende erplite-Backend**|
||vollständiger Code des Frontends, Screenshots und Beschreibung der Funktionalität, Codeerklärungen für implementierte Anwendungsfälle|
|||
|---|---|
|**ABGABE**|08.06.2022 08:00 auf Moodle|
|---|---|

---

<div style="page-break-after: always;"></div>

---
## ***A) Abgabe der Architekturanalyse des bestehenden erplite-Backends***
---
## ***A1) Dokumentation (schriftlich, Codeauszüge, C4-Diagramme, Klassendiagramme) der Ports-Und-Adapters-Architektur von Ordermanagement anhand der gegebenen Anwendungsfälle, die schon implementiert sind:***
---
---

- ..
  - ![Screenshots](images/Screenshot_1.png)
  - ![Screenshots](images/Screenshot_2.png)

---

- **Quellen**
  - [PlantUML - Visual Studio Code Extension zum Erstellen von C4-Diagrammen](https://github.com/plantuml-stdlib/C4-PlantUML#custom-tagsstereotypes-support-and-skinparam-updates )




- ..
  - 

<div style="page-break-after: always;"></div>

---
## ***A2) Dokumentation (schriftlich, Codeauszüge, Diagramme, C4-Diagramme, Klassendiagramme) der "Architektur" von Stockmanagement anhand der gegebenen Anwendungsfälle, die schon implementiert sind:***
---
---

- ..
  - 

---

<div style="page-break-after: always;"></div>

---
## ***B) Abgabe des Frontends für das bestehende erplite-Backend***
---
---

- ..
  - **Aufbau der Routen**
    - ![Screenshots](images/Screenshot_3.png)
    - ![Screenshots](images/Screenshot_4.png)
    - ![Screenshots](images/Screenshot_5.png)
    - ![Screenshots](images/Screenshot_6.png)
    - ![Screenshots](images/Screenshot_7.png)
  - <div style="page-break-after: always;"></div>
  - **Neue Order erstellen**
    - Vordefinierte Kunden und Artikel
      - ![Screenshots](images/Screenshot_8.png)
    - Backend zum Speichern einer Order
      - ![Screenshots](images/Screenshot_9.png)
      - ![Screenshots](images/Screenshot_10.png)
    - Darstellung und Ausgabe der neuen Bestellung über die H2 Console
      - ![Screenshots](images/Screenshot_11.png)
  - <div style="page-break-after: always;"></div>
  - **Buchhaltung - Bestellung auf Verified**
    - Darstellung der Order in der Buchhaltung noch als PLACED
      - ![Screenshots](images/Screenshot_12.png)
      - ![Screenshots](images/Screenshot_13.png)
    - Darstellung nachdem Auf PAYMENT_VERIFIED gestellt wurde
      - ![Screenshots](images/Screenshot_14.png)
  - **Lager - Artikel abhaken**
    - 
      - ![Screenshots](images/Screenshot_15.png)

---